
#include "Partitioning.hpp"
#include <vector>
#include <map>   
#include <cstring>
#include <algorithm> //iota,random_shuffle
#include <cmath>
#include <cassert>
#include <iostream>
#include <ctime>
#include <vector>
#include <list>
using namespace std;

vector<int>* BiFennelBasedPartitioning(int * jobs,int * candidates, int number_of_partition,int nonzeros){

	vector<int> *arry =  new vector<int>[number_of_partition];
	vector<int> *arrySend =  new vector<int>[number_of_partition];

        list<int> la(jobs, jobs+nonzeros);
        la.sort();
        la.unique();
	map<int,vector<int>> neighbour_map;
	vector<int> v{ std::begin(la), std::end(la) };

	for(int i = 0 ; i < la.size(); i++){
	  for(int j = 0 ; j < number_of_partition ; j++){
		neighbour_map[v.at(i)].push_back(0);
	  }
		
	}


	list<int> la2(candidates, candidates+nonzeros);
        la2.sort();
        la2.unique();
	vector<int> candidates_vector{ std::begin(la2), std::end(la2) };
	int candidates2 [candidates_vector.size()];
	for(int i = 0 ; i < candidates_vector.size(); i++){
	  candidates2[i] = candidates_vector.at(i);
	  //cout << candidates2[i] << endl;
	}


 	map<int,vector<int>> adj_map;
	int proc_num_edge [number_of_partition];
	   for(int x = 0 ; x < number_of_partition; x++) proc_num_edge[x] = 0;
	
	//adjacency list
	for(int i = 0 ; i < nonzeros ; i++){
 	      adj_map[candidates[i]].push_back(jobs[i]);
	}



	/*for(int i = 0 ; i < candidates_vector.size() ; i++){
 	      cout << candidates2[i]  << " ";
		for(int j = 0 ; j < adj_map[candidates2[i]].size() ; j++){
		  cout << adj_map[candidates2[i]].at(j) << " " ;
		}
		cout << endl;
	}*/
	
	/*for(int i = 0 ; i < la.size(); i++){
 	      cout << v.at(i)  << " ";
	  for(int j = 0 ; j < number_of_partition ; j++){
		cout << neighbour_map[v.at(i)].at(j) << " ";
	  }
				cout << endl;
	}*/




	for(int i = 0 ; i < candidates_vector.size() ; i++){
	   int proc_degree [number_of_partition];
	   for(int x = 0 ; x < number_of_partition; x++) proc_degree[x] = 0;
			
	   int best_proc = 0;
	   double best_score = -100000;
	   

	//cout << "vertex " << candidates2[i] << " islem goruyor " << endl;
	//cout << "vertex " << candidates2[i] << " in komsu sayisi " << adj_map[candidates2[i]].size() << endl;
	   for(int v = 0 ; v < adj_map[candidates2[i]].size(); v++){
		for(int k = 0 ; k < number_of_partition ; k++){
			int a = neighbour_map[adj_map[candidates2[i]].at(v)].at(k);
		   	proc_degree[k] += a;
			//cout << "proc_degre " << k <<  " "<< a << endl;
		}

	   }

	   for(int k = 0 ; k < number_of_partition ; k++){
	      double proc_score = proc_degree[k] - sqrt(proc_num_edge[k]+adj_map[candidates2[i]].size());
	     	//cout << "proc_score " << proc_score << endl;

	      if(proc_score > best_score){
		 best_score = proc_score;
		 best_proc = k; 
	      }
	   }
	//cout << "best_proc " << best_proc << endl;

	for(int v = 0 ; v < adj_map[candidates2[i]].size(); v++){
		neighbour_map[adj_map[candidates2[i]].at(v)][best_proc] = 1;
		arrySend[best_proc].push_back(candidates2[i]);		
	   }
		arry[best_proc].push_back(candidates2[i]);	
	

	/*for(int i = 0 ; i < la.size(); i++){
 	      cout << v.at(i)  << " ";
	  for(int j = 0 ; j < number_of_partition ; j++){
		cout << neighbour_map[v.at(i)].at(j) << " ";
	  }
				cout << endl;
	}*/


	proc_num_edge[best_proc] += adj_map[candidates2[i]].size();

  	/*for(int j = 0 ; j < number_of_partition ; j++){
		cout << " proc_num_edge "  << j << " "  <<proc_num_edge[j]<< endl;
	  }*/
	
	/*for(int k = 0 ; k < number_of_partition; k++){
		proc_num_edge[k] += adj_map[candidates2[i]].size();
		
	   }*/


	}



	/*for(int i = 0 ; i < number_of_partition; i++){
	   cout << "partition " << i << " size " << arry[i].size()  << endl;
	  	for(int j = 0 ; j < arry[i].size(); j++){
			cout << arry[i].at(j) << endl;	
		}
		
	}*/

	for(int i = 0 ; i < number_of_partition; i++){
	  for(int j = 0 ; j < arry[i].size(); j++){
		 for(int a = 0 ; a < adj_map[arry[i].at(j)].size(); a++){
			arrySend[i].push_back(adj_map[arry[i].at(j)].at(a));
		   }
		}
		
	}

	for(int i = 0 ; i < number_of_partition; i++){
	   cout << "partition " << i << " favorite vertex number " << arry[i].size() << " and edge number " << arrySend[i].size()/2 << endl;
	  	/*for(int j = 0 ; j < arrySend[i].size(); j++){
			cout << arrySend[i].at(j) << endl;	
		}*/

		
	}



	return arrySend;

	

}



vector<int>* AwetoBasedPartitioning(int * jobs,int * candidates, int number_of_partition,int nonzeros){

	vector<int> *arry =  new vector<int>[number_of_partition];
	vector<int> *arrySend =  new vector<int>[number_of_partition];

        list<int> la(jobs, jobs+nonzeros);
        la.sort();
        la.unique();
	map<int,vector<int>> neighbour_map;
	vector<int> v{ std::begin(la), std::end(la) };

	for(int i = 0 ; i < la.size(); i++){
	  for(int j = 0 ; j < number_of_partition ; j++){
		neighbour_map[v.at(i)].push_back(0);
	  }
		
	}


	list<int> la2(candidates, candidates+nonzeros);
        la2.sort();
        la2.unique();
	vector<int> candidates_vector{ std::begin(la2), std::end(la2) };
	int candidates2 [candidates_vector.size()];
	for(int i = 0 ; i < candidates_vector.size(); i++){
	  candidates2[i] = candidates_vector.at(i);
	  //cout << candidates2[i] << endl;
	}


 	map<int,vector<int>> adj_map;
	int proc_num_edge [number_of_partition];
	   for(int x = 0 ; x < number_of_partition; x++) proc_num_edge[x] = 0;



	int BX [number_of_partition];
	   for(int x = 0 ; x < number_of_partition; x++) BX [x] = 1;
	
	//adjacency list
	for(int i = 0 ; i < nonzeros ; i++){
 	      adj_map[candidates[i]].push_back(jobs[i]);
	}



	/*for(int i = 0 ; i < candidates_vector.size() ; i++){
 	      cout << candidates2[i]  << " ";
		for(int j = 0 ; j < adj_map[candidates2[i]].size() ; j++){
		  cout << adj_map[candidates2[i]].at(j) << " " ;
		}
		cout << endl;
	}*/
	
	/*for(int i = 0 ; i < la.size(); i++){
 	      cout << v.at(i)  << " ";
	  for(int j = 0 ; j < number_of_partition ; j++){
		cout << neighbour_map[v.at(i)].at(j) << " ";
	  }
				cout << endl;
	}*/




	for(int i = 0 ; i < candidates_vector.size() ; i++){
	   int proc_degree [number_of_partition];
	   for(int x = 0 ; x < number_of_partition; x++) proc_degree[x] = 0;
			
	   int best_proc = 0;
	   double best_score = -100000;
	   

	//cout << "vertex " << candidates2[i] << " islem goruyor " << endl;
	//cout << "vertex " << candidates2[i] << " in komsu sayisi " << adj_map[candidates2[i]].size() << endl;
	   for(int v = 0 ; v < adj_map[candidates2[i]].size(); v++){
		for(int k = 0 ; k < number_of_partition ; k++){
			int a = neighbour_map[adj_map[candidates2[i]].at(v)].at(k);
		   	proc_degree[k] += a;
			//cout << "proc_degre " << k <<  " "<< a << endl;
		}

	   }

	   for(int k = 0 ; k < number_of_partition ; k++){
	      double proc_score = proc_degree[k] - sqrt(proc_num_edge[k])*BX[k];
	     	//cout << "proc_score " << proc_score << endl;

	      if(proc_score > best_score){
		 best_score = proc_score;
		 best_proc = k; 
	      }
	   }
	//cout << "best_proc " << best_proc << endl;

	for(int v = 0 ; v < adj_map[candidates2[i]].size(); v++){
		neighbour_map[adj_map[candidates2[i]].at(v)][best_proc] = 1;
		arrySend[best_proc].push_back(candidates2[i]);		
	   }
		arry[best_proc].push_back(candidates2[i]);	
	

	/*for(int i = 0 ; i < la.size(); i++){
 	      cout << v.at(i)  << " ";
	  for(int j = 0 ; j < number_of_partition ; j++){
		cout << neighbour_map[v.at(i)].at(j) << " ";
	  }
				cout << endl;
	}*/


	proc_num_edge[best_proc] += adj_map[candidates2[i]].size();

  	/*for(int j = 0 ; j < number_of_partition ; j++){
		cout << " proc_num_edge "  << j << " "  <<proc_num_edge[j]<< endl;
	  }*/
	
	/*for(int k = 0 ; k < number_of_partition; k++){
		proc_num_edge[k] += adj_map[candidates2[i]].size();
		
	   }*/


	}



	/*for(int i = 0 ; i < number_of_partition; i++){
	   cout << "partition " << i << " size " << arry[i].size()  << endl;
	  	for(int j = 0 ; j < arry[i].size(); j++){
			cout << arry[i].at(j) << endl;	
		}
		
	}*/

	for(int i = 0 ; i < number_of_partition; i++){
	  for(int j = 0 ; j < arry[i].size(); j++){
		 for(int a = 0 ; a < adj_map[arry[i].at(j)].size(); a++){
			arrySend[i].push_back(adj_map[arry[i].at(j)].at(a));
		   }
		}
		
	}

	for(int i = 0 ; i < number_of_partition; i++){
	   cout << "partition " << i << " favorite vertex number " << arry[i].size() << " and edge number " << arrySend[i].size()/2 << endl;
	  	/*for(int j = 0 ; j < arrySend[i].size(); j++){
			cout << arrySend[i].at(j) << endl;	
		}*/

		
	}



	return arrySend;

	

}



vector<int>* BiCutBasedPartitioning(int * jobs,int * candidates, int number_of_partition,int nonzeros){

	vector<int> *arry =  new vector<int>[number_of_partition];
	map<int, int> m;
	
	int tmp = 0;
	for(int i = 0; i < nonzeros ; i++){
	  tmp = candidates[i]%number_of_partition;
	  arry[tmp].push_back(candidates[i]);	
	  m[i] = tmp;
	}  

	for(int i = 0; i < nonzeros ; i++){
	  arry[m[i]].push_back(jobs[i]);	
	}  

	return arry;

}




vector<int>* randomPartitioning(int * jobs,int * candidates, int number_of_partition,int nonzeros){

	vector<int> *arry =  new vector<int>[number_of_partition];

	vector<int> v(nonzeros);  // in order to shuffle candidatas and jobs, we create a new vector here
	std::iota(v.begin(), v.end(), 0); //Fills the range (first, last,value) with sequentially increasing values, starting with value and repetitively evaluating ++value   
        std::random_shuffle (v.begin(), v.end()); //shuffle this vector
	
	int tmp = 0;
	for(int i = 0 ; i < nonzeros ; i++){
	  arry[(tmp++)%number_of_partition].push_back(jobs[v.at(i)]);	
	}  
	
	tmp = 0;
	for(int i = 0; i < nonzeros ; i++){
	  arry[(tmp++)%number_of_partition].push_back(candidates[v.at(i)]);	
	}  
	
	return arry;

	

}
vector<int>* hashBasedPartitioning(int * jobs,int * candidates, int number_of_partition,int nonzeros){

	 vector<int> *arry =  new vector<int>[number_of_partition];


	vector<int> cand;


	int tmp = 0;
	for(int i = 0 ; i < nonzeros ; i++){
	  int which_partition = (jobs[i]+candidates[i])%number_of_partition;
	  arry[which_partition].push_back(jobs[i]);
	  cand.push_back(which_partition);
	}  

	for(int i = 0 ; i < nonzeros ; i++){
	  arry[cand.at(i)].push_back(candidates[i]);
	}  
	
	

	
	return arry;

	

}
vector<int>* gridBasedPartitioning(int * jobs,int * candidates, int number_of_partition,int nonzeros){

	static vector<int> *arry =  new vector<int>[number_of_partition];
	vector<int> cand;
	int sq = sqrt(number_of_partition);
	
	assert(sq * sq == number_of_partition && "Number of partitions must be square to use Grid Based Partitioning");
	

	int** grid = new int*[sq];
	int tmp = 0;

	for(int i = 0; i < sq; i++)
	    grid[i] = new int[sq];

	for(int i = 0; i < sq; i++)
	   for(int j = 0; j < sq; j++)
		grid[i][j] = tmp++;


	for(int i = 0 ; i < nonzeros ; i++){
	   int part1 = (jobs[i] % number_of_partition);

	   int x1 = ceil((double) part1 / sq);
	   if(x1 == 0) x1 = sq - 1;
	   else x1 = x1 - 1;

	   int y1 = part1 - (x1 * sq) - 1;

	   vector<int> partition1;

	   for(int a = 0; a < sq; a++)
		partition1.push_back(grid[x1][a]);

	   for(int b = 0; b < sq; b++){
	 	partition1.push_back(grid[b][y1]);
	   
	   }

	   int part2 = (candidates[i] % number_of_partition);

	   int x2 = ceil((double) part2 / sq);
	   if(x2 == 0) x2 = sq - 1;
	   else x2 = x2 - 1;

	   int y2 = part2 - (x2 * sq) - 1;

	   vector<int> partition2;

	   for(int e = 0; e < sq; e++)
		partition2.push_back(grid[x2][e]);

	   for(int f = 0; f < sq; f++)
		partition2.push_back(grid[f][y2]);

	  vector<int> common_elements;
	  sort(partition1.begin(), partition1.end());
  	  sort(partition2.begin(), partition2.end());
          set_intersection(partition1.begin(),partition1.end(),partition2.begin(),partition2.end(),back_inserter(common_elements));

	  srand(std::time(0)); 

	  int randomIndex = rand() % common_elements.size();
	  int which_partition = common_elements.at(randomIndex);
	  arry[which_partition].push_back(jobs[i]);
	  cand.push_back(which_partition);

	
	}

	for(int i = 0 ; i < nonzeros ; i++){
	  arry[cand.at(i)].push_back(candidates[i]);
	}  


	return arry;

	

}
