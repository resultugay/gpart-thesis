#include <stdio.h>
#include <iostream>
#include <fstream>
#include <unistd.h>

#include <netdb.h>
#include <netinet/in.h>

#include <string.h>
#include <stdlib.h> 
#include <pthread.h>

#include <array>

#include <thread>
#include <vector>
#include "Partitioning.hpp"


#define BUFFER_SIZE 32000

using std::endl;
using std::cout;

using namespace std;

void SendJobsTF(vector<int>& threadid);
size_t number_of_candidates, number_of_jobs, nonzeros; //declaretions of numbers
int main(int argc, char *argv[]){

///////////////////////////////////////////////////           FILE         ////////////////////////////////////////////////////////////////

	int weights;  // weights // in future will be used

	std::ifstream file(argv[1]); // first command-line argument is taken as name of input //constructe calls the open file.
	file >> number_of_candidates >> number_of_jobs >> nonzeros; // first line contains number of cand,job and nonzeros.
	cout << "Number of candidates : " << number_of_candidates << "  Number of jobs : " <<  number_of_jobs << " Nonzeros :  " << nonzeros << endl;


	int number_of_connections = atoi(argv[2]);
	cout << "Number of expected connections is : " << number_of_connections << endl;

	// create dynamic array of candidates and jobs.
	//It declares a pointer to a dynamic array of type int
	//int *candidates = new int[number_of_candidates]; 
	//int * jobs = new int[number_of_jobs + 1];	
	//int jobs[nonzeros];
	//int candidates[nonzeros];
	int *jobs = new int[nonzeros];
	int *candidates = new int[nonzeros];



	if(file.is_open()){ //read if file is openable

           for(int i = 0; i < nonzeros; i++) //read up to number nonzeros
              {
                file >> jobs[i] >> candidates[i]; // first element is for candidates and second one is for jobs.
              }
  
        }

	cout << "data was read " << endl;
	/* 	 

	for(int j = 0 ; j < 10 ; j++)
	   cout << candidates[j] << "  "  << jobs[j] << endl; //printing array.
	*/
///////////////////////////////////////////////////           FILE         ////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////           SOCKET       ////////////////////////////////////////////////////////////////
	 socklen_t clilen;
	 int sockfd, newsockfd, portno;
  	 struct sockaddr_in serv_addr, cli_addr;
  	 int  n;

  	/* First call to socket() function */

	/* int socket (int family, int type, int protocol); */
	/*
	family  : AF_INET		IPv4 protocols
	type    : SOCK_STREAM	  	Stream socket
	protocol: 0 (select the system's default) or use followings : 
		IPPROTO_TCP	TCP transport protocol
		IPPROTO_UDP	UDP transport protocol
		IPPROTO_SCTP	SCTP transport protocol
	*/
  	 sockfd = socket(AF_INET, SOCK_STREAM, 0);
	
	if (sockfd < 0) {
	      perror("ERROR opening socket");
	      exit(1);
	   }

	 /* Initialize socket structure */
	 /* The bzero function places nbyte null bytes in the string s. 
	 This function is used to set all the socket structures with null values.*/
	 bzero((char *) &serv_addr, sizeof(serv_addr));
	 portno = 5002; // port number ,can be changed later on.
	 /*
	   struct sockaddr_in {
	      short int            sin_family;  //AF_INET generally
	      unsigned short int   sin_port;    //16-bit port
	      struct in_addr       sin_addr;    //32-bit IP
	      unsigned char        sin_zero[8]; //You just set this value to NULL as this is not being used.
	   };
	   A 0 value for port number means that the system will choose a random port, 
	   and INADDR_ANY value for IP address means the server's IP address will be assigned automatically.
	 */
	 serv_addr.sin_family = AF_INET;
	 serv_addr.sin_addr.s_addr = INADDR_ANY;
	 serv_addr.sin_port = htons(portno); 
	 // host to network short https://stackoverflow.com/questions/19207745/htons-function-in-socket-programing 

	 /*Now bind the host address using bind() call.*/
	 /*
	   int bind(int sockfd, struct sockaddr *my_addr,int addrlen);
	   This call returns 0 if it successfully binds to the address, otherwise it returns -1 on error.

	   sockfd − It is a socket descriptor returned by the socket function.	   
	   my_addr − It is a pointer to struct sockaddr that contains the local IP address and port.
	   addrlen − Set it to sizeof(struct sockaddr).
	 */
	 if (bind(sockfd, (struct sockaddr *) &serv_addr, sizeof(serv_addr)) < 0) {
	    perror("ERROR on binding");
	    exit(1);
	 }
	 /*  int listen(int sockfd,int backlog); 
	     This call returns 0 on success, otherwise it returns -1 on error.
	     
	     sockfd − It is a socket descriptor returned by the socket function.
	     backlog − It is the number of allowed connections.	
	 */

	 listen(sockfd,number_of_connections);
	 clilen = sizeof(cli_addr);
	   
	 /* Accept actual connection from the client */
	 /* int accept (int sockfd, struct sockaddr *cliaddr, socklen_t *addrlen);
	    This call returns a non-negative descriptor on success, otherwise it returns -1 on error.
	    The returned descriptor is assumed to be a client socket descriptor and all read-write operations will be 
            done on this descriptor to communicate with the client.

	    sockfd − It is a socket descriptor returned by the socket function.
	    cliaddr − It is a pointer to struct sockaddr that contains client IP address and port.
	    addrlen − Set it to sizeof(struct sockaddr).	
	
	 */

///////////////////////////////////////////////// THREAD/////////////////////////////////////////////////////

	//vector<int>* arry = randomPartitioning(jobs,candidates,number_of_connections,nonzeros);
	//vector<int>* arry = hashBasedPartitioning(jobs,candidates,number_of_connections,nonzeros);
	//vector<int>* arry = gridBasedPartitioning(jobs,candidates,number_of_connections,nonzeros);
	//vector<int>* arry = BiCutBasedPartitioning(jobs,candidates,number_of_connections,nonzeros);
	//vector<int>* arry = BiFennelBasedPartitioning(jobs,candidates,number_of_connections,nonzeros);
	vector<int>* arry = AwetoBasedPartitioning(jobs,candidates,number_of_connections,nonzeros);

	for(int i = 0 ; i < number_of_connections ; i++){
	  arry[i].insert(arry[i].begin(),arry[i].size()/2 + 1); // here number of jobs is inserted in front of each vector

	}/////////////////////////////////////////////////////////////////////////////////////////////////////////////
  	std::thread threads_jobs[number_of_connections];
	int tmp_connections = 0;
	while(tmp_connections < number_of_connections){
	  newsockfd = accept(sockfd, (struct sockaddr*)&cli_addr, &clilen);
	  if (newsockfd < 0) {
	     perror("ERROR on accept");
	     exit(1);
	  }
	  arry[tmp_connections].insert(arry[tmp_connections].begin(),newsockfd); // here socket number is inserted in front of each vector
	  threads_jobs[tmp_connections] = std::thread(SendJobsTF,ref(arry[tmp_connections])) ;
	  tmp_connections++;
	}

	 for (int i = 0; i < number_of_connections; i++){
             threads_jobs[i].join();
    	 }

	 /* If connection is established then start communicating */
	 /*The bzero function places nbyte null bytes in the string s. This function is used to set all the socket structures with null values.
	  void bzero(void *s, int nbyte);
	  Similar to htons() function.
	 */
	 //bzero(buffer,8);
	 /*
	  The read function attempts to read nbyte bytes from the file associated with the buffer, fildes, into the buffer pointed to by buf.
	  You can also use recv() function to read data to another process.

	  int read(int fildes, const void *buf, int nbyte);

	  fildes − It is a socket descriptor returned by the socket function.    
	  buf − It is the buffer to read the information into. 
	  nbyte − It is the number of bytes to read.
	 
  	  for(int i = 0 ; i < 8 ; i = i + 8){
	    n = read( newsockfd,&buffer[i],8);
	   }
	  
	 if (n < 0) {
	    perror("ERROR reading from socket");
	    exit(1);
	 }
	    for(int i = 0 ; i < 8 ; i = i + 1 ){
	   printf("%d\n",buffer[i]);
	   }
	 */
	   
	 /* Write a response to the client */
	 /*
	  The write function attempts to write nbyte bytes from the buffer pointed by buf 
	  to the file associated with the open file descriptor, fildes.
	  int write(int fildes, const void *buf, int nbyte);

	  fildes − It is a socket descriptor returned by the socket function.
	  buf − It is a pointer to the data you want to send.
          nbyte − It is the number of bytes to be written. If nbyte is 0, write() will return 0
	  and have no other results if the file is a regular file; otherwise, the results are unspecified.

	 */
	// n = write(newsockfd,jobs,50*sizeof(int));



/*

	   int i = 0;
 	   for(i = 0 ; i < nonzeros-(BUFFER_SIZE/sizeof(int)) ; i = i + (BUFFER_SIZE/sizeof(int))){
	      n = write(newsockfd,&jobs[i],BUFFER_SIZE);
	   }
 	   printf("%d\n",i);
	   n = write(newsockfd,&jobs[i],(nonzeros-i)*sizeof(int));

	   if (n < 0) {
	      perror("ERROR writing to socket");
	      exit(1);
	   }


*/

	/*
	 The shutdown function is used to gracefully close the communication between the client and the server.
	 This function gives more control in comparison to the close function. Given below is the syntax of shutdown 
  	 int shutdown(int sockfd, int how);

	 sockfd − It is a socket descriptor returned by the socket function.

	 how − Put one of the numbers −

	 0 − indicates that receiving is not allowed,
 
	 1 − indicates that sending is not allowed, and

	 2 − indicates that both sending and receiving are not allowed. When how is set to 2, it's the same thing as close().
*/
	

	
	if((shutdown(sockfd,2)) != 0)
	   printf("Error Error shutdown socket\n");

///////////////////////////////////////////////////           SOCKET        ////////////////////////////////////////////////////////////////

 	pthread_exit(NULL);
	return 0;

}

void SendJobsTF(vector<int>& threadid) {

      int *jobs = threadid.data();

      long newsockfd;

      newsockfd = jobs[0];
      
      int nonzeros = jobs[1];
      
      int nnzerosTmp = nonzeros - 1;

      int n = write(newsockfd,&nnzerosTmp,sizeof(int)); // number of nonzeros is sent
      int i = 0;
 	   for(i = 2 ; i < 2*nonzeros-(BUFFER_SIZE/sizeof(int)) ; i = i + (BUFFER_SIZE/sizeof(int))){
	      n = write(newsockfd,&jobs[i],BUFFER_SIZE);

		 if (n < 0) {
	          cout << "error at " << i << endl;
	          cout << "jobs" << jobs[i + 1] << endl;
		   	     //send till accepted
			     while(!(n < 0) ){
			      n = write(newsockfd,&jobs[i],BUFFER_SIZE);
			      cout << "error at " << i << endl;
				  }
	 	
	    	   cout << "Problem solved!" << endl;
	         }
	      //printf("i %d \n", i);
	   }
	  n = write(newsockfd,&jobs[i],(2*nonzeros-i)*sizeof(int));

	   if (n < 0) {
	      perror("ERROR writing to socket");
	      exit(1);
	   }

	//printf("%d\n",jobs[269650 + 269645]);

/*
   int i = 0;
   for( i = 0 ; i < nonzeros ; i = i + 1){
	    n = write(newsockfd,&jobs[i],sizeof(int));
	    if (n < 0) {
	      perror("ERROR writing to socket");
	      exit(1);
	    }

   }



*/

      if((shutdown(newsockfd,2)) != 0)
	printf("Error Error shutdown socket\n");

}

