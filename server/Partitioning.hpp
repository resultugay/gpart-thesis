#ifndef _partitioning_h
#define _partitioning_h

#include <vector>

using namespace std;

vector<int>* randomPartitioning(int * jobs,int * candidates, int number_of_partition,int nonzeros);
vector<int>* hashBasedPartitioning(int * jobs,int * candidates, int number_of_partition,int nonzeros);
vector<int>* gridBasedPartitioning(int * jobs,int * candidates, int number_of_partition,int nonzeros);
vector<int>* BiCutBasedPartitioning(int * jobs,int * candidates, int number_of_partition,int nonzeros);
vector<int>* BiFennelBasedPartitioning(int * jobs,int * candidates, int number_of_partition,int nonzeros);
vector<int>* AwetoBasedPartitioning(int * jobs,int * candidates, int number_of_partition,int nonzeros);
#endif
