#include <iostream>
#include "Point.hpp"

using namespace std;

int Point::number_of_dimensions = 0;

Point::Point(long int p_id,double* p_data,int p_cluster_id){
 data = new double[1];
 id = p_id;
 for(int i = 0 ; i < number_of_dimensions ; i++){
    data[i] = p_data[i];
 }
 cluster_id = p_cluster_id;
}
Point::Point(){

}

long int Point::get_id(){
	return id;
}
short int Point::get_cluster_id(){
	return cluster_id;
}
double * Point::get_data(){
	return data;
}

void Point::set_cluster_id(int p_cluster_id){
	this->cluster_id = p_cluster_id;
}
