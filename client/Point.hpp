#ifndef POINTS_H
#define POINTS_H

#include <vector>
using namespace std;

class Point{

  private:
	long int id;
	short int cluster_id;
	double *data;

  public:
        static int number_of_dimensions;
	Point(long int p_id,double* p_data,int p_cluster_id);
	Point();
	long int get_id();
	short int get_cluster_id();
	double * get_data();
        void set_cluster_id(int p_cluster_id);

};
#endif
