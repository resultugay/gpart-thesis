#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <netdb.h>
#include <netinet/in.h>
#define BUFFER_SIZE 32000
#include <string.h>
#include <string>
#include <iostream>
#include <vector>
#include <map>
#include "Point.hpp"
#include "Kmeans.hpp"
#include <fstream>


using std::string;
using std::cout;
using std::endl;
using std::vector;
using std::map;

int main(int argc, char *argv[]) {
   int sockfd, portno, n;
   struct sockaddr_in serv_addr;
   struct hostent *server;
   
   int nonzeros [1] = {0};	
   
   if (argc < 1) {
      fprintf(stderr,"usage %s hostname port\n", argv[0]);
      exit(0);
   }
	
   portno = 5002;//atoi(argv[2]);
   
   /* Create a socket point */
   sockfd = socket(AF_INET, SOCK_STREAM, 0);
   
   if (sockfd < 0) {
      perror("ERROR opening socket");
      exit(1);
   }
	
   server = gethostbyname("127.0.0.1"/*argv[1]*/);
   
   if (server == NULL) {
      fprintf(stderr,"ERROR, no such host\n");
      exit(0);
   }
   
   bzero((char *) &serv_addr, sizeof(serv_addr));
   serv_addr.sin_family = AF_INET;
   bcopy((char *)server->h_addr, (char *)&serv_addr.sin_addr.s_addr, server->h_length);
   serv_addr.sin_port = htons(portno);
   
   /* Now connect to the server */
   if (connect(sockfd, (struct sockaddr*)&serv_addr, sizeof(serv_addr)) < 0) {
      perror("ERROR connecting");
      exit(1);
   }
   
   /* Now ask for a message from the user, this message
      * will be read by server
   */
	
   //printf("Please enter the message: ");
   //bzero(jobs,8);
   //fgets(jobs,8,stdin);

   //printf("Here is the message: %d\n",jobs[3]);
   
   /* Send message to the server */
   //for(int i = 0 ; i < 8 ; i = i + 8){ 
    //n = write(sockfd, &jobs[i], 8);
   //}
	
   //bzero(jobs,*nonzeros*sizeof(int));

  n = read(sockfd,nonzeros,sizeof(int)); //nonzeros is taken
  printf("nonzeros : %d\n",nonzeros[0]);
  int *jobs = new int[nonzeros[0]];
  int *candidates = new int[nonzeros[0]];

  int i = 0;
   for( i = 0 ; i < nonzeros[0]-(BUFFER_SIZE/sizeof(int)); i = i + (BUFFER_SIZE/sizeof(int))){
	    n = read(sockfd,&jobs[i],BUFFER_SIZE);
	    if (n < 0) {
	      perror("ERROR writing to socket");
	      exit(1);
	    }
   }

	   n = read(sockfd,&jobs[i],(nonzeros[0]-i)*sizeof(int));
	

    for( i = 0 ; i < nonzeros[0]-(BUFFER_SIZE/sizeof(int)); i = i + (BUFFER_SIZE/sizeof(int))){
	    n = read(sockfd,&candidates[i],BUFFER_SIZE);
	    if (n < 0) {
	      perror("ERROR writing to socket");
	      exit(1);
	    }
   }
	   n = read(sockfd,&candidates[i],(nonzeros[0]-i)*sizeof(int));

	//printf("%d\n",candidates[269645]);
   if (n < 0) {
      perror("ERROR writing to socket");
      exit(1);
   }

/////////////////////////////////////////////////////////////MAPPING///////////////////////////////////////////////////////////////
   int *jobs_mapped = new int[nonzeros[0]];
   map<int , int> m;

   m[jobs[0]] = 1;https://resultugay@bitbucket.org/resultugay/gpart-thesis.git
   int count = 2;

   for(int i = 1; i < nonzeros[0] ; i++){
	if(m[jobs[i]] == 0)
	   m[jobs[i]] = count++;
   }
	

   for(int i = 0; i < nonzeros[0] ; i++){
	jobs_mapped[i] = m[jobs[i]];
   }
   int jobs_count = count - 1;
 ///////////////////////////////////////////////////
   int *candidates_mapped = new int[nonzeros[0]];
   map<int , int> m2;

   m2[candidates[0]] = 1;
   count = 2;

   for(int i = 1; i < nonzeros[0] ; i++){
	if(m2[candidates[i]] == 0)
	   m2[candidates[i]] = count++;
   }

   for(int i = 0; i < nonzeros[0] ; i++){
	candidates_mapped[i] = m2[candidates[i]];
   }
     int candidates_count = count - 1;
/////////////////////////////////////////////////////////////MAPPING///////////////////////////////////////////////////////////////





	FILE *file;
	std::string filenameTmp;
	filenameTmp = "output.txt";// +  std::to_string(sockfd) ;
	vector<char> v(filenameTmp.begin(), filenameTmp.end());
	char * filename = &v[0]; 
	cout << filenameTmp << endl;

	if((file=fopen(filename, "wb"))==NULL)
	{
	    printf("Something went wrong reading %s\n", "test.txt");
	    return 0;
	}
	else
	{        fprintf(file,"%s","%%MatrixMarket matrix coordinate real symmetric\n%Generated 18-Sep-2017\n");
		 fprintf(file, "%d %d %d\n",jobs_count,candidates_count,nonzeros[0]);
 		 for(int i = 0; i < nonzeros[0]; i++)
       	 	   fprintf(file, "%d %d 1\n",jobs_mapped[i],candidates_mapped[i]);
		   //printf("%d %d \n",jobs[i],jobs_mapped[i]);
	}
	fclose(file);
	printf("Data was written to test.txt file\n");

	if((shutdown(sockfd,2)) != 0)
	   printf("Error shutdown socket\n");



	system("/home/rsltgy/gpart-thesis/graphchi/toolkits/collaborative_filtering/svd --training=./output.txt --nsv=2 --nv=4 --max_iter=5 --quiet=1 --tol=1e-1");
	cout << "ok" << endl;

///////////////////////////////////POINT//////////////////////////////////////////////


	Point::number_of_dimensions = 2;
	vector<Point> points;
	const char * output_jobs = "output.txt_U.mm";

///////////////////////////////////// svd_u ////////////////////////////////////////////
	std::ifstream file2(output_jobs);
 	string tmp;
	int tmp2;
	for(int i = 0 ; i < 3 ; i++)getline( file2, tmp );//First unnecessary two lines 

  	//double *jobs_candidates_svd = new double[jobs_count+candidates_count];
        map<long int , double*> jobs_candidates_svd;

	if(file2.is_open()){ //read if file is openable

           for(int i = 0; i < jobs_count; i++) 
              {
		double *tmp3 = new double;
		for(int j = 0 ; j < Point::number_of_dimensions ; j++ ){
       		   file2 >> tmp3[j] ; 
		}
		//jobs_candidates_svd[i] = tmp3;	
		Point* t = new Point(i,tmp3,1);
		points.push_back(*t);

			
              }
  
        }
	file2.close();
///////////////////////////////////// svd_u /////////////////////////////////////////////
///////////////////////////////////// svd_v ////////////////////////////////////////////
	const char* output_candidates = "output.txt_V.mm";
	file2.open(output_candidates);
	for(int i = 0 ; i < 3 ; i++)getline( file2, tmp );//First unnecessary two lines 

	if(file2.is_open()){ //read if file2 is openable

           for(int i = jobs_count ; i < jobs_count + candidates_count; i++) 
              {
		double *tmp3 = new double;
		for(int j = 0 ; j < Point::number_of_dimensions ; j++ ){
       		   file2 >> tmp3[j] ; 
		}
		//jobs_candidates_svd[i] = tmp3;	
		Point* t = new Point(i,tmp3,1);
		points.push_back(*t);

              }
  
        }
	file2.close();
///////////////////////////////////// svd_v /////////////////////////////////////////////
////////////////////////////////////PRINT RESULT/////////////////////////////////////////
	/*for(int i = 0 ; i < jobs_count ; i++){
		
		//cout << points.at(i).get_data() << endl;
		double *tmp3 = points.at(i).get_data();
		for(int j = 0 ; j < Point::number_of_dimensions ; j++ ){
       		   cout << tmp3[j] << " "; 
		}			
		   cout << endl;	
		
	}*/				
////////////////////////////////////PRINT RESULT/////////////////////////////////////////		
///////////////////////////////////POINT/////////////////////////////////////////////////
        //cout << "size: " << points.size() << '\n';
        //cout << "jobs + candidates: " << jobs_count  + candidates_count<< '\n';

	Kmeans kmeans;
	kmeans.run(points,Kmeans::EUCLID_DISTANCE,4,2,1000);
	
	for(int i = 0 ; i < 10 ; i++){
		cout  << i << " ) ---> cluster " << points.at(i).get_cluster_id() << endl;
	}

return 0;

}
