#ifndef KMEANS_H
#define KMEANS_H

#include <vector>
#include "Point.hpp"

class Kmeans{

public:
    static int EUCLID_DISTANCE;
    Kmeans();
    vector<Point>* run(vector<Point> &points,int distance_metric, int number_of_cluster,int number_of_dimensions,int number_of_iteration);
    long double euclidean_distance(double *p1, double *p2);
    double* find_new_center_points(vector<Point> &points);

};
#endif
