#include <vector>
#include "Kmeans.hpp"
#include "Point.hpp"
#include <iostream>
#include <ctime>
#include <cstdlib>
#include<math.h>

int Kmeans::EUCLID_DISTANCE = 1;

Kmeans::Kmeans(){
	EUCLID_DISTANCE = 1;
}

vector<Point>* Kmeans::run(vector<Point> &points,int distance_metric, int number_of_cluster,int number_of_dimensions,int number_of_iteration){
	vector<Point> *arry =  new vector<Point>[number_of_cluster];
	if(distance_metric == EUCLID_DISTANCE){
	   //cout << "distance metric is euclid and points size is "<< points.size() << endl;
	   srand(static_cast <unsigned int> (time(0)));
	   //int randNum = rand()%(max-min + 1) + min;
	   double** centers = new double*[number_of_cluster];
	   for(int i = 0 ; i < number_of_cluster ; i++){
	   	centers[i] = points.at(rand()%(points.size()-0 + 1)).get_data();
  	   }

		for(int r = 0 ; r < number_of_iteration ; r++){
		   for(int i = 0 ; i < points.size(); i++){
			int cluster = 0;
			long double min_distance = euclidean_distance(points.at(i).get_data(),centers[0]);  
			for(int j = 1; j < number_of_cluster; j++){
	 			long double min = euclidean_distance(points.at(i).get_data(),centers[j]);
				if(min < min_distance){
				 min_distance  = min;
				 cluster = j;
				}
			}
	 		points.at(i).set_cluster_id(cluster);
			arry[cluster].push_back(points.at(i));
		   }
		        double centers_converged = 0;

			for(int c = 0 ; c < number_of_cluster ; c++){
			   for(int d = 0 ; d < number_of_dimensions ; d++){
				   centers_converged += centers[c][d];				
				}
			}


			for(int i = 0 ; i < number_of_cluster ; i++){
				centers[i] = find_new_center_points(arry[i]);
			}

			double new_centers_converged = 0;

			for(int c = 0 ; c < number_of_cluster ; c++){
			   for(int d = 0 ; d < number_of_dimensions ; d++){
				   new_centers_converged += centers[c][d];				
				}
			}
		
			if(fabs(new_centers_converged - centers_converged) < 10e-5){
			   cout << "Kmeans converged at " << r << " iteration" << endl;
			   break;
			}



		}

			for(int i = 0 ; i < number_of_cluster ; i++){
				cout << i << " .cluster ";
				for(int j = 0 ; j < Point::number_of_dimensions; j++){
					cout << centers[i][j] << " " ;					
				}
				cout << " " << endl;
			}
		


	}
}

long double Kmeans::euclidean_distance(double *p1, double *p2){

	long double dist = 0; 
	for(int i = 0 ; i < Point::number_of_dimensions ; i++ ){
	    //cout << " p1 " << p1[i] << " p2 " << p2[i] << " dist  "  << pow((p1[i] - p2[i]),2) << endl;  
	    dist += pow((p1[i] - p2[i]),2);
	}
	return sqrt(dist);
}

double* Kmeans::find_new_center_points(vector<Point> &points){
	double *new_centers = new double[Point::number_of_dimensions];
	for(int i = 0 ; i < Point::number_of_dimensions; i++){
	   for(int j = 0 ; j < points.size(); j++){
		new_centers[i] += points.at(j).get_data()[i];
	   }
	}	
	for(int i = 0 ; i < Point::number_of_dimensions; i++){
		new_centers[i] /= points.size();
	}
	
	return new_centers;
}


